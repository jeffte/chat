import { useNavigate } from "react-router-dom";

import { useAuth } from "../../hooks/useAuth";

import { signInContainer, heading, signInButton } from "./styles.module.css";

export function SignIn() {
	const { signInWithGoogle } = useAuth();

	const navigate = useNavigate();

	async function handleSignIn() {
		try {
			await signInWithGoogle();

			navigate("/new-room");
		} catch (error) {
			console.log(error);

			const firebaseError = error;

			if (firebaseError?.code !== "auth/popup-closed-by-user") {
				alert("Erro ao realizar login");
			}
		}
	}

	return (
		<main className={signInContainer}>
			<p className={heading}>Clique no botão para fazer login</p>
			<button type="button" className={signInButton} onClick={handleSignIn}>
				Login com Google
			</button>
		</main>
	);
}
