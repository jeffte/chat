import { collection, serverTimestamp, addDoc } from "firebase/firestore";
import { useNavigate } from "react-router-dom";

import { firestore } from "../../services/firebase";

import {
	newRoomContainer,
	heading,
	createRoomButton,
} from "./styles.module.css";

export function NewRoom() {
	const navigate = useNavigate();

	async function handleCreateRoom() {
		const roomRef = collection(firestore, "chats");
		const newRoomRef = await addDoc(roomRef, {
			createdAt: serverTimestamp(),
			finished: false,
		});

		navigate(`/chat/${newRoomRef.id}`);
	}

	return (
		<div className={newRoomContainer}>
			<h1 className={heading}>
				Clique no botão baixo para criar uma nova sala
			</h1>

			<button
				className={createRoomButton}
				type="button"
				onClick={handleCreateRoom}
			>
				Criar nova sala
			</button>
		</div>
	);
}
