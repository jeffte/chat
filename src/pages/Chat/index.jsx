import { useState, useRef, useEffect } from "react";
import { PaperPlaneRight, User } from "@phosphor-icons/react";
import { useParams } from "react-router-dom";
import {
	onSnapshot,
	collection,
	addDoc,
	serverTimestamp,
	query,
	orderBy,
} from "firebase/firestore";

import { firestore } from "../../services/firebase";

import { useAuth } from "../../hooks/useAuth";

import {
	homeContainer,
	header,
	userInfo,
	userStatus,
	status,
	profile,
	messagesContainer,
	messages,
	messageBubble,
	body,
	you,
	inputWrapper,
	messageForm,
} from "./styles.module.css";

export function Chat() {
	const inputRef = useRef(null);

	const { id } = useParams();

	const { user } = useAuth();

	const [chats, setChats] = useState([]);

	useEffect(() => {
		const chatRef = collection(firestore, "chats", id, "messages");

		const orderMessagesByTimestamp = query(
			chatRef,
			orderBy("createdAt", "asc"),
		);

		const unsubscribe = onSnapshot(orderMessagesByTimestamp, (snapshot) => {
			setChats(snapshot.docs.map((doc) => doc.data()));
		});

		return () => {
			unsubscribe();
		};
	}, [id]);

	async function handleSendMessage(event) {
		event.preventDefault();

		const chatRef = collection(firestore, "chats", id, "messages");

		const message = inputRef.current.value;

		if (message.trim() === "") {
			return;
		}

		const payload = {
			sender: user.id,
			value: message.trim(),
			createdAt: serverTimestamp(),
		};

		await addDoc(chatRef, payload);

		inputRef.current.value = "";
	}

	return (
		<div className={homeContainer}>
			<header className={header}>
				<div className={userInfo}>
					{user ? (
						<img className={profile} src={user.avatar} alt={user.name} />
					) : (
						<div className={profile}>
							<User size={24} />
						</div>
					)}

					<div className={userStatus}>
						<strong>Atendente</strong>
						<div className={status}>Online</div>
					</div>
				</div>
			</header>

			<div className={messagesContainer}>
				<div className={messages}>
					{chats?.map((chat) => (
						<div
							key={chat.value}
							className={`${messageBubble} ${
								chat.sender === user.id ? you : ""
							}`}
						>
							<div className={body}>{chat.value}</div>
						</div>
					))}
				</div>
			</div>

			<footer>
				<form className={messageForm} onSubmit={handleSendMessage}>
					<div className={inputWrapper}>
						<input
							name="message"
							type="text"
							placeholder="Digite sua mensagem"
							ref={inputRef}
						/>
						<button type="submit" title="Enviar">
							<PaperPlaneRight size={24} />
						</button>
					</div>
				</form>
			</footer>
		</div>
	);
}
