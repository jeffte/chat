import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { collection, getDocs } from "firebase/firestore";

import { firestore } from "../../services/firebase";

import { roomsContainer, heading, chatContainer } from "./styles.module.css";

export function Rooms() {
	const [chats, setChats] = useState([]);

	async function getChats() {
		const querySnapshot = await getDocs(collection(firestore, "chats"));

		setChats(querySnapshot.docs.map((doc) => doc.id));
	}

	useEffect(() => {
		getChats();
	}, []);

	return (
		<div className={roomsContainer}>
			<h1 className={heading}>Chats</h1>

			{chats?.map((chat) => (
				<Link key={chat} className={chatContainer} to={`/chat/${chat}`}>
					{chat}
				</Link>
			))}
		</div>
	);
}
