import { BrowserRouter, Route, Routes } from "react-router-dom";
import { SignIn } from "./pages/SignIn";
import { Chat } from "./pages/Chat";
import { Rooms } from "./pages/Rooms";
import { NewRoom } from "./pages/NewRoom";

export default function Router() {
	return (
		<BrowserRouter>
			<Routes>
				<Route path="/" element={<SignIn />} />

				{/* create new room */}
				<Route path="/new-room" element={<NewRoom />} />

				{/* authenticated routes */}
				<Route path="/chat/:id" element={<Chat />} />

				{/* list all rooms */}
				<Route path="/rooms" element={<Rooms />} />
			</Routes>
		</BrowserRouter>
	);
}
