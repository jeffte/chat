import Router from "./Router";
import { AuthContextProvider } from "./contexts/AuthContext";

export function App() {
	return (
		<AuthContextProvider>
			<Router />
		</AuthContextProvider>
	);
}
