import { createContext, useState, useEffect } from "react";

import { signOut, signInWithPopup, onAuthStateChanged } from "firebase/auth";
import { auth, googleAuthProvider } from "../services/firebase";

export const AuthContext = createContext({});

export function AuthContextProvider({ children }) {
	const [user, setUser] = useState(null);

	async function signInWithGoogle() {
		const response = await signInWithPopup(auth, googleAuthProvider);

		if (response.user) {
			const { displayName, photoURL, uid } = response.user;

			if (!displayName) {
				throw new Error("A conta do google está incompleta");
			}

			setUser({
				id: uid,
				name: displayName,
				avatar: photoURL,
			});
		}
	}

	async function signUserOut() {
		await signOut(auth);
	}

	useEffect(() => {
		const unsubscribe = onAuthStateChanged(auth, (user) => {
			if (user) {
				const { displayName, photoURL, uid } = user;

				if (!displayName || !photoURL) {
					throw new Error("Missing information from Google Account.");
				}

				setUser({
					id: uid,
					name: displayName,
					avatar: photoURL,
				});
			}
		});

		return () => {
			unsubscribe();
		};
	}, []);

	return (
		<AuthContext.Provider value={{ user, signInWithGoogle, signUserOut }}>
			{children}
		</AuthContext.Provider>
	);
}
